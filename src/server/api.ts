// require('dotenv').config();
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.router.js";

import { errorLogger, error_handler, error_handler2, not_found, urlNotFoundHandler } from "./middleware/errors.handler.js";   
import { errorLogFilePath, reqLogFilePath } from "./utils.js";
import { reqLogger } from "./middleware/logger.js";
import { generateReqId } from "./middleware/reqIdGenerator.js";


// =============================================
//              CONVERT TO CLASS
// =============================================


const { PORT = 8080,HOST = "localhost", DB_URI } = process.env;

const app = express();

// middleware
app.use(cors());  // allows to determine who can access our server. in this case - everyone has access.
app.use(morgan("dev"));
app.use(reqLogger(reqLogFilePath));
app.use(generateReqId);


// routing
// app.use('/api/stories', story_router);
// app.use('/api/meetings', meeting_router);
app.use("/api/users", user_router);

// central error handling - MINE 
app.use(urlNotFoundHandler);
app.use(errorLogger(errorLogFilePath));
app.use(error_handler);
app.use(error_handler2);


/*
// central error handling
app.use(error_handler);
app.use(error_handler2);

//when no routes were matched...
app.use("*", not_found);
*/


//start the express api server
;(async ()=> {
  //connect to mongo db
  await connect_db(DB_URI as string);  
  await app.listen(Number(PORT), HOST);
  log.magenta("api is live on",` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log);