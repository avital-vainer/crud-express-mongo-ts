import * as yup from "yup";
import {  } from "./user.model.js";


// all user fields are required
const createSchema = yup.object().shape({
    first_name: yup.string().required(),
    last_name: yup.string().required(),
    email: yup.string().email().required(),
    phone: yup.string().matches(/\d{3}-\d{3}-\d{4}/).required(), 
    // "\d{3}-\d{3}-\d{4}" is a regex of a phone number that looks like this: ###-###-####
});

// all user fields are optional
const updateSchema = yup.object().shape({
    first_name: yup.string(),
    last_name: yup.string(),
    email: yup.string().email(),
    phone: yup.string().matches(/\d{3}-\d{3}-\d{4}/),
});

export const CREATE_USER = "CREATE_USER";
export const UPDATE_USER = "UPDATE_USER";

export const validateUserData = async(userData: unknown, reqType = UPDATE_USER) => {
    if (reqType === CREATE_USER) {
        return await createSchema.isValid(userData);
    }

    return await updateSchema.isValid(userData);
};
