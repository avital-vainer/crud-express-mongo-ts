import fs from "fs/promises";
import { NextFunction, Request, Response } from "express";


export const reqLogger = (logFilePath: string) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await fs.appendFile(logFilePath, `${req.method} ${req.originalUrl} ${Date.now()}\n`, "utf-8");
        next();
    };
};
