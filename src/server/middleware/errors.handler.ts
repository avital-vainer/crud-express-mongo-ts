import log from "@ajar/marker";
import fs from "fs/promises";
import { Request, Response, NextFunction } from "express";
import UrlNotFoundException from "../exceptions/UrlNotFoundException.js";
import { HttpException } from "../exceptions/HttpException.js";
import { IErrorResponse } from "../utils.js";
const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;

export const error_handler =  (err: Error, req: Request, res: Response, next: NextFunction) => {
    log.error(err);
    next(err);
};

export const error_handler2 =  (err: Error, req: Request, res: Response) => {
    if(NODE_ENV !== "production")res.status(500).json({status:err.message,stack:err.stack});
    else res.status(500).json({status:"internal server error..."});
};

export const not_found =  (req: Request, res: Response) => {
    log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
};



// ========================================================
// ========================================================

export function urlNotFoundHandler(req: Request, res: Response, next: NextFunction) {
    next(new UrlNotFoundException(req.originalUrl));
}

export const errorLogger = (logFilePath: string) => {

    return async (err: HttpException, req: Request, res: Response, next: NextFunction) => {
        await fs.appendFile(logFilePath, `# ${req.id} --> ${err.status} : ${err.message} >> ${err.stack} \n`, "utf-8");
        next(err);
    };
};

export function printAndRespondError(error: HttpException, req: Request, res: Response) {
  const errRes: IErrorResponse = { 
    status: error.status || 500, 
    msg: error.message || "Something went wrong",
    stack: error.stack
  };

  log.red(`Error: ${errRes.msg}`);
  res.status(errRes.status).json(errRes);
}